class UI {
	constructor() {
		this.btnName = document.getElementById("btn-name");
		this.inputName = document.getElementById("txt-name");
		// Form
		this.jokeForm = document.getElementById("joke-form");
		this.comboCategories = document.getElementById("select-categories");
		this.responseMessage = document.getElementById("example-message");
		this.btnSubmit = document.querySelector("input[type=submit]");
		//
		this.apiservices = new APIService();
	}

	showText() {
		const sText = this.inputName.value;
		console.log(sText);
		this.inputName.value = "";
	}

	fillCombo(jsonData, objCombo) {
		// empty option for select
		const emptyOption = document.createElement("option");
		const txt = document.createTextNode("");
		emptyOption.setAttribute("value", "");
		emptyOption.appendChild(txt);
		objCombo.appendChild(emptyOption);
		//
		jsonData.forEach(cat => {
			const newOption = document.createElement("option");
			const newValueTxt = document.createTextNode(cat); // cat.name / cat.description etc
			newOption.setAttribute("value", cat); // cat.id, etc
			newOption.appendChild(newValueTxt);
			objCombo.appendChild(newOption);
			// console.log(cat);
		});
	}

	// Api Service methods
	async fillCategoriesCombo() {
		try {
			const response = await this.apiservices.getCategories(); // Devuelve un Array no un json como tal
			if (response) {
				this.fillCombo(response, this.comboCategories);
			}
		} catch (error) {
			console.error(error);
		}
	}

	async getJoke() {
		try {
			const response = await this.apiservices.getJokeByCategory();
			if (response) {
				this.responseMessage.value = response.value;
			}
		} catch (error) {
			console.error(error);
		}
	}

	// Mejorar esto con activar desactivar botón
	setCategory(sCat) {
		this.apiservices.setCategory(sCat);
		this.responseMessage.value = "";
		if (sCat && sCat !== "") {
			this.btnSubmit.classList.remove("button-primary-disabled");
			this.btnSubmit.classList.add("button-primary");
			this.btnSubmit.disabled = false;
		} else {
			this.btnSubmit.classList.remove("button-primary");
			this.btnSubmit.classList.add("button-primary-disabled");
			this.btnSubmit.disabled = true;
		}
	}
}
