class APIService {
	constructor() {
		this.category = "";
		this.baseURL = "https://api.chucknorris.io/jokes";
		this.categoriesURL = `${this.baseURL}/categories`;
		this.jokesxcategURL = "";
		//
		let misCabeceras = new Headers();

		this.miInit = {
			method: "GET",
			headers: misCabeceras,
			mode: "cors",
			cache: "default"
		};
	}

	async getCategories() {
		const response = await fetch(this.categoriesURL);
		return await response.json();
	}

	async getJokeByCategory() {
		const response = await fetch(this.jokesxcategURL, this.miInit);
		return await response.json();
	}

	setCategory(sCat) {
		this.category = sCat;
		this.jokesxcategURL = `${this.baseURL}/random?category=${this.category}`;
	}
}
