function eventListeners() {
	const ui = new UI();
	//
	ui.fillCategoriesCombo();
	//
	ui.btnName.addEventListener("click", event => {
		event.preventDefault();
		ui.showText();
	});

	ui.comboCategories.addEventListener("change", event => {
		ui.setCategory(event.target.value);
		// console.log(event.target.value);
	});

	ui.jokeForm.addEventListener("submit", event => {
		event.preventDefault();
		ui.getJoke();
	});
}

document.addEventListener("DOMContentLoaded", () => {
	eventListeners();
});
